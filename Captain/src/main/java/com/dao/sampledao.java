package com.dao;
import java.util.*;

import javax.servlet.http.Part;

import java.security.MessageDigest;
import java.sql.*;


import com.bean.Sample;
import com.controller.LoginTest;
import com.controller.Valid_User;

import org.mindrot.jbcrypt.BCrypt;
public class sampledao {
	public void print(Object line) {
		System.out.println(line);
	}
	public ArrayList<Sample> getProducts() {
		Sample p = new Sample();
		ArrayList<Sample> ans = new ArrayList<>();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://db:3306/jarvis","root","admin");
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("select * from products");
			while(rs.next()) {
				//System.out.println("hi");
				//ans.add(new Products(101,"shaik","twttw",9.8,2));
				int id = rs.getInt("productId");
				String name = rs.getString("productName");
				String desc = rs.getString("productDescription");
				double price = rs.getDouble("productPrice");
				int quant = rs.getInt("productQuantity");
				String category = rs.getString("productCategory");
				p = new Sample(id,name,desc,price,quant,category);
				ans.add(p);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return ans;
	}
	public int  postProducts(Sample prod) {
		int val = 0;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://db:3306/jarvis","root","admin");
			PreparedStatement st = con.prepareStatement("INSERT INTO products(productId,productName,productDescription,productPrice,productQuantity,productCategory) VALUES(?,?,?,?,?,?)" );
			st.setInt(1,prod.getProductId());
			st.setString(2, prod.getProductName());
			st.setString(3, prod.getProductDescription());
			st.setDouble(4, prod.getProductPrice());
			st.setInt(5, prod.getProductQuantity());
			st.setString(6, prod.getCategory());
			val = st.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}
		return val;
	}
	
	public int DeleteProducts(int prod_id) {
		int val = 0;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://db:3306/jarvis","root","admin");
			PreparedStatement st = con.prepareStatement("DELETE FROM products WHERE productId = ?");
			st.setInt(1, prod_id);
			val = st.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return val;
	}
	public int UpdateProducts(int prod_id,String prod_name,String prod_desc,int prod_price,int prod_quant,String category) {
		int val = 0;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://db:3306/jarvis","root","admin");
			PreparedStatement st = con.prepareStatement("UPDATE products SET productName = ?,productDescription = ?,productPrice = ?,productQuantity = ?,productCategory = ? WHERE productId = ?");
			st.setString(1, prod_name);
			st.setString(2, prod_desc);
			st.setDouble(3, prod_price);
			st.setInt(4, prod_quant);
			st.setString(5, category);
			st.setInt(6, prod_id);
			val = st.executeUpdate();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return val;
		
	}
	
	public int signup(String email, String userName, String password) {
		int status = 0;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://db:3306/jarvis","root","admin");		
			PreparedStatement preparedStatement = con.prepareStatement("SELECT * FROM users WHERE userName = ? or email = ?");
			 boolean flag = true;
		         preparedStatement.setString(1, userName);
		         preparedStatement.setString(2, email);
		         ResultSet resultSet = preparedStatement.executeQuery();
		         if (resultSet.next()) {
		            // The username is already in use
		        	
		        	 if(resultSet.getString(1).equals(email) && resultSet.getString(2).equals(userName)) {
		        		 //System.out.println("ALready Registered ");
		        		 status = 2;
		        	 }
		        	 else if(resultSet.getString(2).equals(userName) && resultSet.getString(1) != email) {
		        		 //Try Another username
		        		 status = 3;
		        	 }
		        	 else if(resultSet.getString(1).equals(email) && resultSet.getString(2) != userName) {
		        		 //Email is already Taken
		        		 status = 4;
		        	 }
		         }
		         else {
		        	 
		       String hashpassword = BCrypt.hashpw(password, BCrypt.gensalt());
		        	 
		     PreparedStatement insert = con.prepareStatement("INSERT INTO users(email,userName,password) VALUES(?,?,?)");
			insert.setString(1, email);
			insert.setString(2, userName);
			insert.setString(3, hashpassword);
			status = insert.executeUpdate();
			//System.out.println(status);
		         }
		         
		    
		}catch(Exception e) {
			e.printStackTrace();
		}
		return status;
	}
	 public boolean checkPassword(String password, String hash) {
	        return BCrypt.checkpw(password, hash);
	    }
	public String login(String userNameorEmail, String Enteredpassword){
		String  status = "";
		try
		{
			Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con = DriverManager.getConnection("jdbc:mysql://db:3306/jarvis","root","admin");		
		PreparedStatement st = con.prepareStatement("select password,email,userName from users where userName = ? or email = ?");
		st.setString(1, userNameorEmail);
		st.setString(2, userNameorEmail);
		//st.setString(3, Enteredpassword);
		ResultSet rs = st.executeQuery();
		if(rs.next()) {
				if(checkPassword(Enteredpassword,rs.getString(1))) {
					status = "Ok" + rs.getString(2) + rs.getString(3);
						//login Successfull
					}
					else {
								status = "Not_Invalid_Password";
									//Invalid Password
			
						}
		}
		else {
			status = "Invalid_UserName_Or_Password";
			//Invalid Username or Password
			
		}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return status;
	}
	public int addtocart(int product_id,int cart_quantity,String cart_user) {
		int status = 0;
		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://db:3306/jarvis","root","admin");
			PreparedStatement ps = con.prepareStatement("select * from Bag where product_Id= ? and personName = ?");	
			ps.setInt(1, product_id);
			ps.setString(2, cart_user);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				int old_quant = rs.getInt(2);
				System.out.println(old_quant);
				int new_quant = old_quant + cart_quantity;
				System.out.println(new_quant);
				PreparedStatement us = con.prepareStatement("UPDATE Bag SET cartQuantity = ? WHERE product_Id= ?");
				us.setInt(1, new_quant);
				us.setInt(2, product_id);
				status = us.executeUpdate();
				System.out.print("Item updated succcessfully ");
				
			}
			else {
				
				PreparedStatement st = con.prepareStatement("INSERT INTO Bag(product_Id,cartQuantity,personName) VALUES(?,?,?)");
				st.setInt(1, product_id);
				st.setInt(2,cart_quantity);
				st.setString(3, cart_user);
				status = st.executeUpdate();
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return status;
				
	}
	public int deletefromcart(int product_id,String user_name) {
		int status = 0;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://db:3306/prod","root","Root");
			PreparedStatement st = con.prepareStatement("SELECT * FROM products WHERE productId= ?");
			st.setInt(1, product_id);
			ResultSet rs = st.executeQuery();
			if(rs.next()) { 
				PreparedStatement ps = con.prepareStatement("DELETE FROM Bag WHERE product_Id = ? and personName = ?");
				ps.setInt(1, product_id);
				ps.setString(2, user_name);
				status = ps.executeUpdate();
				System.out.println(status);
				print(status);
			}
			else {
				status = 0;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return status;
	}
	public List getCartItems(String userName) {
		List<HashMap<String,String>> list = new ArrayList<HashMap<String,String>>();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://db:3306/prod","root","Root");
			PreparedStatement st = con.prepareStatement("select personName,productId,productName,productDescription,productPrice,productCategory,cartQuantity from products p JOIN Bag b ON p.productId = b.product_Id WHERE personName = ?");
			st.setString(1, userName);
			ResultSet rs = st.executeQuery();
			while(rs.next()) {
					HashMap<String,String> map = new HashMap<String,String>();
					map.put("personName",rs.getString(1));
			        map.put("productId", Integer.toString(rs.getInt(2)));
			        map.put("productName", rs.getString(3));
			        map.put("productDescription", rs.getString(4));
			        map.put("productPrice", Integer.toString(rs.getInt(5)));
			        map.put("productCategory", rs.getString(6));
			        map.put("cartQuantity", Integer.toString(rs.getInt(7)));
			        list.add(map);
				
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	
	
}

