package com.s3.util;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import java.util.ArrayList;
import java.util.List;


public class ImageFetch extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public ImageFetch() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();

        String bucketName = "your-bucket-name";
        ObjectListing objectListing = s3Client.listObjects(bucketName);

        List<String> imageUrls = new ArrayList<>();
        for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
            String imageUrl = s3Client.getUrl(bucketName, objectSummary.getKey()).toString();
            imageUrls.add(imageUrl);
        }

        response.setContentType("application/json");
        response.getWriter().write(imageUrls.toString());
    }

}
