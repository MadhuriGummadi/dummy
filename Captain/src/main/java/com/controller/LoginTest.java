package com.controller;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.sampledao;
import com.google.gson.Gson;
import com.security.*;
/**
 * Servlet implementation class LoginTest
 */
public class LoginTest extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginTest() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "*");
		response.setContentType("application/json");
		String username = request.getParameter("userNameOrEmail");
		String password = request.getParameter("password");
		PrintWriter pw = response.getWriter();
		sampledao dao = new sampledao();
		//pw.write(dao.login(username, password));
		if(dao.login(username,password).contains("Ok")) {
				Valid_User user = new Valid_User();
				user.setUserName(username);
				//pw.write(user.getUserName());
			 	String s = dao.login(username, password);
		       String email = " ";
		       int break_point = 0;
		       for(int i = 0;i<s.length();i++){
		           if(s.charAt(i) == '.'){
		               email = s.substring(2,i + 4);
		               break_point = i + 4;
		           }
		       }
		        String uname = s.substring(break_point, s.length());
		        //pw.write(uname);
		        Gson gson = new Gson();
				JWTDAO token = new JWTDAO();
		      //  pw.write('\n');
		        String mytoken = token.generateToken();
		    	//pw.write(token.generateToken());
				//pw.write(gson.toJson(token.generateToken()));
		    HashMap<String, String > list = new HashMap<>();
		       list.put("username",uname);
		       list.put("email",email);
		       list.put("jwtToken", mytoken );
			//pw.write(token.generateToken());
		//	pw.write(gson.toJson(token.generateToken()));
			
			pw.write(gson.toJson(list));
    		//pw.write("Login Succesfull");
    	}
    	else if(dao.login(username, password).equals("Not_Invalid_Password")){
    		pw.write("Invalid Password");
    	}
    	else {
    		pw.write("Invalid UserName or Email");
    	}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	

}
