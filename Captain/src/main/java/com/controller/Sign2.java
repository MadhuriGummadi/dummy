package com.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.sampledao;

/**
 * Servlet implementation class Sign2
 */
public class Sign2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Sign2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "*");
			sampledao dao = new sampledao();
			response.setContentType("application/json");
			String email = request.getParameter("email");
			String username = request.getParameter("userName");
			String password = request.getParameter("password");
			int val = dao.signup(email, username, password);
			PrintWriter pw = response.getWriter();
			switch(val) {
			case 1:
			{
				pw.write("Registration successfull");
				break;
			}
			case 2:
			{
				pw.write("Already Registered");
				break;
			}
			case 3:
			{
				pw.write("Change Username");
				break;
			}
			case 4:
			{
				pw.write("Email already taken");
				break;
			}
			default:
				pw.write("Fail");
			}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	

}
