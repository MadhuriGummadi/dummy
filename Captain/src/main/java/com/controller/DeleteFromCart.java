package com.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.sampledao;

/**
 * Servlet implementation class DeleteFromCart
 */
public class DeleteFromCart extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteFromCart() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			int product_id = Integer.parseInt(request.getParameter("productId"));
			String user_name = request.getParameter("userName");
			response.setContentType("application/json");
			sampledao dao = new sampledao();
			PrintWriter pw = response.getWriter();
			if(dao.deletefromcart(product_id,user_name) > 0) {
				pw.write("Product Deleted From Cart");
			}
			else {
				pw.write("Fail");
			}
			
	}

}
