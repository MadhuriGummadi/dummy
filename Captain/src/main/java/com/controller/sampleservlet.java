package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bean.Sample;
import com.dao.sampledao;
import com.google.gson.Gson;

/**
 * Servlet implementation class sampleservlet
 */
@WebServlet(urlPatterns = {"/sampleservlet"} ,name = "sampleservlet",description = "NOne")

public class sampleservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public sampleservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "*");
		sampledao obj = new sampledao();
		ArrayList<Sample> list = new ArrayList<>();
		list = obj.getProducts();
		Gson gson = new Gson();
		String user = gson.toJson(list);
		PrintWriter pw = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF - 8");
		System.out.println(list);
		pw.write(user);
		pw.close();
//     String url = "http://localhost:8080/Captain/sampleservlet";
//		response.encodeUrl(url);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
}
