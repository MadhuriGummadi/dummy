package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bean.Sample;
import com.dao.sampledao;
import com.google.gson.Gson;

/**
 * Servlet implementation class Postservlet
 */
public class Postservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Postservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "*");
		String productDescription = request.getParameter("productDescription");
		String encodedProductDescription = URLEncoder.encode(productDescription, "UTF-8");
//		String target = "/Captain/Postservlet?productId=21&productName=mask&productDescription=" + encodedProductDescription + "&productPrice=9.8&productQuantity=1";
		sampledao obj = new sampledao();
		int id = Integer.parseInt(request.getParameter("productId"));
		String name = request.getParameter("productName");
		String desc = request.getParameter("productDescription");
		double prod_price = Double.parseDouble(request.getParameter("productPrice"));
		int prod_quant = Integer.parseInt(request.getParameter("productQuantity"));
		String category = request.getParameter("category");
		Sample prod = new Sample(id,name,desc,prod_price,prod_quant,category);
		PrintWriter pw = response.getWriter();
		
		if(obj.postProducts(prod) > 0) {
			pw.print("Inserted SuccessFully");
			
		}
		else {
			pw.print("Failed");
			
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF  - 8");
		pw.close();
		
		doGet(request, response);
		
	}

}
