package com.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.sampledao;

/**
 * Servlet implementation class AddtoCart
 */
public class AddtoCart extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddtoCart() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			int product_id = Integer.parseInt(request.getParameter("product_Id"));
			int cart_quantity = Integer.parseInt(request.getParameter("cart_quantity"));
			String user_name = request.getParameter("userName");
			response.setContentType("application/json");
			PrintWriter pw = response.getWriter();
			sampledao dao = new sampledao();

			String cart_user = user_name;
			if(dao.addtocart(product_id, cart_quantity,cart_user) == 1) {
				pw.write("Item added to cart");
			}
			else if(dao.addtocart(product_id, cart_quantity,cart_user) == 2){
				pw.write("Updated successfully");
			}
			else {
				pw.write("Fail");
			}
			
	}
	

}
