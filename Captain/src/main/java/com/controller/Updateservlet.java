package com.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.sampledao;

/**
 * Servlet implementation class Updateservlet
 */
public class Updateservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Updateservlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    public void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {
    	response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "*");
		int id = Integer.parseInt(request.getParameter("productId"));
		String name = request.getParameter("productName");
		String desc = request.getParameter("productDescription");
		double price = Double.parseDouble(request.getParameter("productPrice"));
		int quant = Integer.parseInt(request.getParameter("productQuantity"));
		String category = request.getParameter("category");
		PrintWriter pw = response.getWriter();
		sampledao dao = new sampledao();
		if(dao.UpdateProducts(id, name, desc, id, quant,category) > 0 ) {
			pw.write("Updated Successfully");
		}
		else {
				pw.write("Failed to Update");
		}
		pw.close();
		response.setCharacterEncoding("UTF - 8");
		response.setContentType("application/json");
		
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
