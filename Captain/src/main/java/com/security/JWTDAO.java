package com.security;

import java.sql.Date;
import java.util.Base64;
import java.util.concurrent.TimeUnit;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTDAO {
		public String generateToken() {
			String id = "ISO123";
			String key = "MY_SECRET_KEY";
			String subject = "ProoductMangement";
			String token = Jwts.builder().setId(id).setSubject(subject).setIssuer("Ironman")
					.setIssuedAt(new Date(System.currentTimeMillis()))
					.setExpiration(new Date(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(20)))
					.signWith(SignatureAlgorithm.HS256,Base64.getEncoder().encode(key.getBytes()))
					.compact();
					return token;
		}
}
